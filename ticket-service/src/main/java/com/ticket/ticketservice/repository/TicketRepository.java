package com.ticket.ticketservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ticket.ticketservice.domain.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {
	
	Ticket findTicketByNumeroPremiado(Integer numeroPremiado);
	
	Ticket findAllByProdutoid(Integer produtoId);

}
